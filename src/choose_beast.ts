import { browser, Tabs } from "webextension-polyfill-ts";

require("webextension-polyfill-ts");
require("scss/main.scss");


/**
 * CSS to hide everything on the page,
 * except for elements that have the "beastify-image" class.
 */
const hidePage = `body > :not(.beastify-image) {
	display: none;
  }`;


/**
 * Given the name of a beast, get the URL to the corresponding image.
 */
function beastNameToURL(beastName: string): string {
	switch (beastName) {
	case "Frog":
		return browser.extension.getURL("beasts/frog.jpg");
	case "Snake":
		return browser.extension.getURL("beasts/snake.jpg");
	case "Turtle":
		return browser.extension.getURL("beasts/turtle.jpg");
	default:
		throw new Error("Unimplemented");
	}
}

/**
 * Insert the page-hiding CSS into the active tab,
 * then get the beast URL and
 * send a "beastify" message to the content script in the active tab.
 */
async function beastify(tabs: Tabs.Tab[], beastName: string) {
	await browser.tabs.insertCSS({ code: hidePage });

	const url = beastNameToURL(beastName);
	browser.tabs.sendMessage(tabs[0].id!, {
		command: "beastify",
		beastURL: url
	});
}

/**
 * Remove the page-hiding CSS from the active tab,
 * send a "reset" message to the content script in the active tab.
 */
async function reset(tabs: Tabs.Tab[]) {
	await browser.tabs.removeCSS({ code: hidePage });
	browser.tabs.sendMessage(tabs[0].id!, {
		command: "reset",
	});
}

/**
 * Just log the error to the console.
 */
function reportError(error: Error | string) {
	console.error(`Could not beastify: ${error}`);
}


/**
* Listen for clicks on the buttons, and send the appropriate message to
* the content script in the page.
*/
function listenForClicks() {
	const handleEvent = async (e: MouseEvent) => {
		const target = e.target as HTMLElement;

		// Get the active tab, then call "beastify()" or "reset()" as appropriate.
		if (target.classList.contains("beast")) {
			const tabs = await browser.tabs.query({ active: true, currentWindow: true });
			beastify(tabs, target.textContent!);
		} else if (target.classList.contains("reset")) {
			const tabs = await browser.tabs.query({ active: true, currentWindow: true })
			reset(tabs);
		}
	}

	document.addEventListener("click", (e) => handleEvent(e).catch(reportError));
}

/**
* There was an error executing the script.
* Display the popup's error message, and hide the normal UI.
*/
function reportExecuteScriptError(error: Error) {
	document.querySelector("#popup-content")!.classList.add("hidden");
	document.querySelector("#error-content")!.classList.remove("hidden");
	console.error(`Failed to execute beastify content script: ${error.message}`);
}

/**
* When the popup loads, inject a content script into the active tab,
* and add a click handler.
* If we couldn't inject the script, handle the error.
*/
browser.tabs.executeScript({ file: "/beastify.js" })
	.then(listenForClicks)
	.catch(reportExecuteScriptError);
