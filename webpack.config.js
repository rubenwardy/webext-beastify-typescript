const path = require("path");
const CopyPlugin = require("copy-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const HtmlWebpackPlugin = require("html-webpack-plugin");

const isProd = process.env.NODE_ENV === "production";
const dest = path.resolve(__dirname, "dist");

const mode = isProd ? "production" : "development";

console.log(`Webpack is building in ${mode}`);

module.exports = {
	mode: mode,
	entry: {
		choose_beast: "./src/choose_beast",
		beastify: "./src/beastify",
	},
	devtool: isProd ? undefined : "source-map",
	plugins: [
		new CopyPlugin({
			patterns: [
				{ from: "src/public/", to: path.resolve(__dirname, "dist/") },
			]
		}),
		new MiniCssExtractPlugin({
			// Options similar to the same options in webpackOptions.output
			// both options are optional
			filename: "[name].css",
			chunkFilename: "[id].css",
		}),
		new HtmlWebpackPlugin({
			filename: "choose_beast.html",
			title: "Choose Beast",
			template: "src/templates/choose_beast.ejs",
			chunks: [ "choose_beast" ],
			hash: true,
		})
	],
	module: {
		rules: [
			{
				test: /\.tsx?$/,
				use: [
					{
						loader: "ts-loader",
						options: {
							configFile: "tsconfig.json"
						},
					},
				],
				exclude: /node_modules/
			},
			{
				test: /\.s?[ac]ss$/i,
				use: [
					MiniCssExtractPlugin.loader,
					"css-loader",
					"sass-loader"
				],
				sideEffects: true,
			},
		]
	},
	resolve: {
		extensions: [".ts", ".tsx", ".js"],
		modules: [
			path.resolve(__dirname, "src"),
			"node_modules"
		]

	},
	output: {
		filename: "[name].js",
		path: dest
	},
};
